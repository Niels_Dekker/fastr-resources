#!/bin/bash

function usage {
    echo "Usage: ${BASH_SOURCE[0]} --input <input_image> --dimension 2D/3D --kernel_size 7 --output <output_image>"
    echo "Error: $1"
    exit 1
}

module load fsl/5.0.9

while [[ $# -gt 1 ]] ; do
    key="$1"

    case $key in
        --input)
            input_image="$2"
            shift # past argument
        ;;
        --dimension)
            dimension="$2"
            shift # past argument
        ;;
        --kernel_size)
            kernel_size="$2"
        ;;
        --output)
            output_image="$2"
        ;;
        *)
            # unknown option
        ;;
    esac
    shift # past argument or value
done

[[ -z $input_image ]] && usage "No input image provided"
[[ -z $dimension ]] && usage "No dimension provided"
[[ -z $kernel_size ]] && usage "No kernel_size provided"
[[ -z $output_image ]] && usage "No output image location provided"

fslmaths $input_image -kernel ${dimension}D -kernel sphere $size -eroF -dilF ${output_image}
