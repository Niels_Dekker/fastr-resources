import fastr
import os
print fastr.toollist
network = fastr.Network('test_ROBEX_with_OASIS')
robexExtractor = network.create_node('ROBEX.ROBEX', id_='robexExtractor' )
sourceImage = network.create_source('ITKImageFile', id_='sourceImage')
strippedImage = network.create_sink('ITKImageFile', id_='strippedImage')
# Link the inputs and outputs
robexExtractor.inputs['inputImageFile'] = sourceImage.output
strippedImage.inputs['input'] = robexExtractor.outputs['strippedImageFile']
# Configure the source and sink data
source_data = {'sourceImage': 'vfsregex://oasis/.*\.nii\.gz'}
sink_data = {'strippedImage': 'vfs://oasis/stripped/stripped_{sample_id}.nii.gz'}
network.draw_network()
network.execute(source_data, sink_data, cluster_queue='ProcessPoolExecution')
