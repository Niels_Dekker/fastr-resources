#!/usr/bin/env python

# Copyright 2011-2014 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import fastr


def create_network():
    network = fastr.Network(id_="multi_template_seg")

    source_target = network.create_source(datatype=fastr.typelist['ITKImageFile'], id_='target_img')
    source_template = network.create_source(datatype=fastr.typelist['ITKImageFile'], id_='template_img', nodegroup='atlas')
    source_mask = network.create_source(datatype=fastr.typelist['ITKImageFile'], id_='mask_image', nodegroup='atlas')
    source_params = network.create_source(datatype=fastr.typelist['ElastixParameterFile'], id_='param_file')

    elastix_node = network.create_node(fastr.toollist['Elastix'], id_='elastix')
    elastix_node.inputs['fixed_image'] = source_target.output
    elastix_node.inputs['moving_image'] = source_template.output
    elastix_node.inputs['moving_image'].input_group = 'atlas'
    link_param = network.create_link(source_params.output, elastix_node.inputs['parameters'])
    link_param.collapse = 0

    transformix_node = network.create_node(fastr.toollist['Transformix'], id_='transformix')
    transformix_node.inputs['image'] = source_mask.output
    transformix_node.inputs['transform'] = elastix_node.outputs['transform'][-1]

    combine_node = network.create_node(fastr.toollist['PxCombineSegmentations'], id_='combine')
    link_combine = network.create_link(transformix_node.outputs['image'], combine_node.inputs['images'])
    link_combine.collapse = 'atlas'
    combine_node.inputs['method'] = ['VOTE']
    combine_node.inputs['number_of_classes'] = ['3']

    outimage = network.create_sink(datatype=fastr.typelist['ITKImageFile'], id_='sink_image')
    outimage.inputs['input'] = combine_node.outputs['hard_segment']

    # Print some info.
    #transformix_node.outputs['image'].preferred_types = [fastr.typelist['MetaImageFile'], fastr.typelist['AnalyzeImageFile']]

    return network


def source_data():
    sourcedata = {'template_img': {'template1055': 'vfs://fastr_data/atlas_seg/input/ergomri_1055_mri_0992959_403_T1_NuM_SubLR.nii.gz',
                                   'template316': 'vfs://fastr_data/atlas_seg/input/ergomri_316_mri_2911928_475_T1_NuM_SubLR.nii.gz',
#                                   'template376': 'vfs://fastr_data/atlas_seg/input/ergomri_376_mri_1912816_456_T1_NuM_SubLR.nii.gz',
#                                   'template492': 'vfs://fastr_data/atlas_seg/input/ergomri_492_2913158_126_T1_NuM_SubLR.nii.gz',
#                                   'template52': 'vfs://fastr_data/atlas_seg/input/ergomri_52_0909398_88_T1_NuM_SubLR.nii.gz',
#                                   'template565': 'vfs://fastr_data/atlas_seg/input/ergomri_565_mri_1971689_65_T1_NuM_SubLR.nii.gz',
#                                   'template566': 'vfs://fastr_data/atlas_seg/input/ergomri_566_mri_9927874_559_T1_NuM_SubLR.nii.gz',
#                                   'template636': 'vfs://fastr_data/atlas_seg/input/ergomri_636_mri_9928038_13_T1_NuM_SubLR.nii.gz',
#                                   'template670': 'vfs://fastr_data/atlas_seg/input/ergomri_670_mri_2928910_561_T1_NuM_SubLR.nii.gz',
                                   'template688': 'vfs://fastr_data/atlas_seg/input/ergomri_688_mri_3929034_156_T1_NuM_SubLR.nii.gz'},
                  'target_img': {'target1137': 'vfs://fastr_data/atlas_seg/input/ergomri_1137_m_3994469_446_T1_NuM_SubLR.nii.gz',
#                                 'target86': 'vfs://fastr_data/atlas_seg/input/ergomri_86_2909538_112_T1_NuM_SubLR.nii.gz',
#                                 'target863': 'vfs://fastr_data/atlas_seg/input/ergomri_863_mri_5973967_589_T1_NuM_SubLR.nii.gz',
#                                 'target873': 'vfs://fastr_data/atlas_seg/input/ergomri_873_mri_5973999_549_T1_NuM_SubLR.nii.gz',
#                                 'target881': 'vfs://fastr_data/atlas_seg/input/ergomri_881_mri_2974099_194_T1_NuM_SubLR.nii.gz',
#                                 'target929': 'vfs://fastr_data/atlas_seg/input/ergomri_929_mri_4974249_444_T1_NuM_SubLR.nii.gz',
#                                 'target953': 'vfs://fastr_data/atlas_seg/input/ergomri_953_mri_2974797_490_T1_NuM_SubLR.nii.gz',
                                 'target29': 'vfs://fastr_data/atlas_seg/input/ergomri_29_mri_4953983_67_T1_NuM_SubLR.nii.gz'},
                  'mask_image': {'mask1055': 'vfs://fastr_data/atlas_seg/input/ergomri_1055_mri_0992959_403_hippo_SubLR.nii.gz',
                                 'mask316': 'vfs://fastr_data/atlas_seg/input/ergomri_316_mri_2911928_475_hippo_SubLR.nii.gz',
#                                 'mask376': 'vfs://fastr_data/atlas_seg/input/ergomri_376_mri_1912816_456_hippo_SubLR.nii.gz',
#                                 'mask492': 'vfs://fastr_data/atlas_seg/input/ergomri_492_2913158_126_hippo_SubLR.nii.gz',
#                                 'mask52': 'vfs://fastr_data/atlas_seg/input/ergomri_52_0909398_88_hippo_SubLR.nii.gz',
#                                 'mask565': 'vfs://fastr_data/atlas_seg/input/ergomri_565_mri_1971689_65_hippo_SubLR.nii.gz',
#                                 'mask566': 'vfs://fastr_data/atlas_seg/input/ergomri_566_mri_9927874_559_hippo_SubLR.nii.gz',
#                                 'mask636': 'vfs://fastr_data/atlas_seg/input/ergomri_636_mri_9928038_13_hippo_SubLR.nii.gz',
#                                 'mask670': 'vfs://fastr_data/atlas_seg/input/ergomri_670_mri_2928910_561_hippo_SubLR.nii.gz',
                                 'mask688': 'vfs://fastr_data/atlas_seg/input/ergomri_688_mri_3929034_156_hippo_SubLR.nii.gz'},
                  'param_file': ['vfs://fastr_data/atlas_seg/constant/parStefanAf.txt',
                                 'vfs://fastr_data/atlas_seg/constant/parStefanNR2.txt']}

    return sourcedata


def main():
    network = create_network()

    network.draw_network(img_format='svg', draw_dimension=True)

    sourcedata = source_data()
    sinkdata = {'sink_image': 'vfs://fastr_data/multi_atlas_seg/output/hipposeg_{sample_id}.nii.gz'}

    network.execute(sourcedata, sinkdata)


if __name__ == '__main__':
    main()
