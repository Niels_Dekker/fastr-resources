# -*- coding: utf-8 -*-
"""
Created on Fri May 19 14:15:32 2017

@authors: Seyed Mostafa Kia (m.kia83@gmail.com), Marcel Zwiers (m.zwiers@donders.ru.nl)
"""

#!/usr/bin/env python

import fastr
import shutil

kernel = 2.13         # WM/CSF erosion kernel box <size> : all voxels in a box of width <size> centered on target voxel
bptf   = [69.44, -1]  # Highpass/lowpass filter frequency: high/lowpass_sigma = halfmax / TR = 0.5 / (high/lowpass_freq * TR) => bptf = 0.5 / high/lowpass_freq

def source_data():
    sourcedata = {'fmri_images':   {'S1': '/project/3022017.01/S500-20140625/101006/MNINonLinear/Results/rfMRI_REST1_RL/rfMRI_REST1_RL_hp2000_clean.nii.gz'},
                                    #'S2': '/project/3022017.01/S500-20140625/101006/MNINonLinear/Results/rfMRI_REST2_LR/rfMRI_REST2_LR_hp2000_clean.nii.gz'},
                  'masks':          '/home/mrstats/seykia/Data/ICP/mask_left.nii',
                  'brain_mask':     '/home/mrstats/seykia/Data/ICP/MNI152_T1_2mm_brain_mask.nii.gz',
                  'wm_mask':        '/home/mrstats/seykia/Data/ICP/wm.nii.gz',
                  'csf_mask':       '/home/mrstats/seykia/Data/ICP/ventricles.nii.gz',
                  'scale':          5,
                  'master_setup':   '/home/mrstats/seykia/Data/ICP/rs_fMRI.fsf',
                  't1_image':       '/home/mrstats/seykia/Data/ICP/T1w_acpc_dc.nii.gz',
                  'fmri_tr':        0.72
                 }   
    return sourcedata


def sink_data():
    sinkdata = {'icp_results': 'vfs://home/Results/ICP/test_{sample_id}'}
    shutil.rmtree(sinkdata['icp_results'].replace('vfs://home','/home/mrstats/seykia'), ignore_errors=True)
    return sinkdata


def create_network():
    # Creating the nodes
    network             = fastr.Network(                            id_='ICP_workflow') 
    fmri_source         = network.create_source('DicomImageFile',   id_='fmri_images',          stepid='fmri_preprocessing')
    tr_source           = network.create_source('Float',            id_='fmri_tr',              stepid='fmri_preprocessing')
    feat_source         = network.create_source('FsfFile',          id_='master_setup',         stepid='fmri_preprocessing')
    t1_image            = network.create_source('DicomImageFile',   id_='t1_image',             stepid='fmri_preprocessing')
    bptf_constant       = network.create_constant('Float', bptf,    id_='bandpass_halfmax',     stepid='fmri_preprocessing')
    dcm2nii_node        = network.create_node('DicomToNifti',       id_='dicom_to_nifti',       stepid='fmri_preprocessing')
    dcm2nii_T1_node     = network.create_node('DicomToNifti',       id_='dicom_to_nifti_T1',    stepid='fmri_preprocessing')
    reorient_node       = network.create_node('FSLReorient2Std',    id_='reorient2std',         stepid='fmri_preprocessing')
    feat_node           = network.create_node('Feat',               id_='feat_preprocessing',   stepid='fmri_preprocessing')
    aroma_node          = network.create_node('ICAAroma',           id_='ica_aroma_denoising',  stepid='fmri_preprocessing')
    filter_node         = network.create_node('FSLMaths',           id_='maths_bandpass_smooth',stepid='fmri_preprocessing')
    divide_node         = network.create_node('Divide',             id_='bandpass_sigma',       stepid='fmri_preprocessing')        
    masks               = network.create_source('DicomImageFile',   id_='masks',                stepid='ICP')
    brain_mask          = network.create_source('DicomImageFile',   id_='brain_mask',           stepid='ICP')
    wm_mask             = network.create_source('DicomImageFile',   id_='wm_mask',              stepid='ICP')
    csf_mask            = network.create_source('DicomImageFile',   id_='csf_mask',             stepid='ICP')
    scale               = network.create_source('Int',              id_='scale',                stepid='ICP')
    dcm2nii_masks_node  = network.create_node('DicomToNifti',       id_='dicom_to_nifti_masks', stepid='ICP')
    dcm2nii_brain_node  = network.create_node('DicomToNifti',       id_='dicom_to_nifti_brain', stepid='ICP')
    dcm2nii_wm_node     = network.create_node('DicomToNifti',       id_='dicom_to_nifti_wm',    stepid='ICP')
    dcm2nii_csf_node    = network.create_node('DicomToNifti',       id_='dicom_to_nifti_csf',   stepid='ICP')
    icp_node            = network.create_node('ICP',                id_='icp',                  stepid='ICP')
    icp_sink            = network.create_sink('Directory',          id_='icp_results',          stepid='ICP')

    # Set the datatype of the sources to avoid complains from fastr (this shouldn't be necessary, really (can be figured out at runtime or by propagation))
    dcm2nii_node.outputs['image'].datatype              =   fastr.typelist['NiftiImageFileCompressed']
    dcm2nii_T1_node.outputs['image_cropped'].datatype   =   fastr.typelist['NiftiImageFileCompressed']
    dcm2nii_masks_node.outputs['image'].datatype        =   fastr.typelist['NiftiImageFileCompressed']
    dcm2nii_brain_node.outputs['image'].datatype        =   fastr.typelist['NiftiImageFileCompressed']
    dcm2nii_wm_node.outputs['image'].datatype           =   fastr.typelist['NiftiImageFileCompressed']
    dcm2nii_csf_node.outputs['image'].datatype          =   fastr.typelist['NiftiImageFileCompressed']
    reorient_node.outputs['reoriented_image'].datatype  =   fastr.typelist['NiftiImageFileCompressed']
    divide_node.outputs['result'].datatype              =   fastr.typelist['Float']
    filter_node.inputs['operator2_number'].datatype     =   fastr.typelist['Float']    
    masks.output.datatype                               =   fastr.typelist['NiftiImageFileCompressed']
    brain_mask.output.datatype                          =   fastr.typelist['NiftiImageFileCompressed']
    wm_mask.output.datatype                             =   fastr.typelist['NiftiImageFileCompressed']
    csf_mask.output.datatype                            =   fastr.typelist['NiftiImageFileCompressed']
    
    # Creating the links
    fmri_source.output                                  >>  dcm2nii_node.inputs['dicom_image']
    t1_image.output                                     >>  dcm2nii_T1_node.inputs['dicom_image']
    dcm2nii_node.outputs['image']                       >>  reorient_node.inputs['image']
    reorient_node.outputs['reoriented_image']           >>  feat_node.inputs['fmri_images']
    feat_source.output                                  >>  feat_node.inputs['setup_file']
    tr_source.output                                    >>  feat_node.inputs['TR']
    dcm2nii_T1_node.outputs['image_cropped']            >>  feat_node.inputs['t1_image']   
    tr_source.output                                    >>  divide_node.inputs['right_hand']
    bptf_constant.output                                >>  divide_node.inputs['left_hand']
    feat_node.outputs['feat_directory']                 >>  aroma_node.inputs['feat_directory']
    aroma_node.outputs['denoised_nonaggr_images']       >>  filter_node.inputs['image1']
    ('-bptf',)                                          >>  filter_node.inputs['operator1']
    (divide_node.outputs['result']                      >>  filter_node.inputs['operator1_number']).collapse = 'bandpass_halfmax'
    (kernel,)                                           >>  filter_node.inputs['operator2_number']
    ('-s',)                                             >>  filter_node.inputs['operator2']
    filter_node.outputs['output_image']                 >>  icp_node.inputs['fmri_images']
    masks.output                                        >>  dcm2nii_masks_node.inputs['dicom_image']    
    dcm2nii_masks_node.outputs['image']                 >>  icp_node.inputs['masks']
    brain_mask.output                                   >>  dcm2nii_brain_node.inputs['dicom_image']    
    dcm2nii_brain_node.outputs['image']                 >>  icp_node.inputs['brain_mask']    
    wm_mask.output                                      >>  dcm2nii_wm_node.inputs['dicom_image']    
    dcm2nii_wm_node.outputs['image']                    >>  icp_node.inputs['wm_mask']    
    csf_mask.output                                     >>  dcm2nii_csf_node.inputs['dicom_image']    
    dcm2nii_csf_node.outputs['image']                   >>  icp_node.inputs['csf_mask']    
    scale.output                                        >>  icp_node.inputs['parcellation_scale']
    icp_node.outputs['output_dir']                      >>  icp_sink.input
    
    return network


def main():
    sourcedata = source_data()
    sinkdata   = sink_data()
    network    = create_network()
    network.draw_network(name = network.id, draw_dimension = True)
    network.execute(sourcedata, sinkdata, tmpdir='/home/mrstats/seykia/temp/ICP')


if __name__ == '__main__':
    main()
