#!/usr/bin/env python

import fastr
import shutil
# import glob

# Defaults
CMA_labels = {'Left-Thalamus-Proper':     10,
              'Left-Caudate':             11,
              'Left-Putamen':             12,
              'Left-Pallidum':            13,
              'Brain-Stem_4th-Ventricle': 16,
              'Left-Hippocampus':         17,
              'Left-Amygdala':            18,
              'Left-Accumbens-area':      26,
              'Right-Thalamus-Proper':    49,
              'Right-Caudate':            50,
              'Right-Putamen':            51,
              'Right-Pallidum':           52,
              'Right-Hippocampus':        53,
              'Right-Amygdala':           54,
              'Right-Accumbens-area':     58}


def source_data():
    # S1_T1      = tuple(f.replace('/home/mrphys/marzwi','vfs://home') for f in glob.glob("/home/mrphys/marzwi/MATRICS/dropbox/001-001/MRI/02_t1_structural/*.dcm"))
    # S2_T1      = tuple(f.replace('/home/mrphys/marzwi','vfs://home') for f in glob.glob("/home/mrphys/marzwi/MATRICS/dropbox/001-003/MRI/14_t1_structural/*.dcm"))
    sourcedata = {'t1_source':   {'S1': 'vfs://home/MATRICS/dropbox/001-001/MRI/02_t1_structural',
                                  'S2': 'vfs://home/MATRICS/dropbox/001-003/MRI/14_t1_structural'},
                  'label_source': CMA_labels}
    sourcedata = {'t1_source':   ['xnat://xnat.bmia.nl/search?projects=bbmri-wp3-dev&resources=DICOM&type=t1_mprage_PH8_grappa2&subject=DICOD%&insecure=true&id=subject&label=true'],
                  'label_source': CMA_labels}
    return sourcedata


def sink_data():
    
    sinkdata = {'first_seg_image':     'vfs://home/tmp/BBMRI/fastr_subcortical_segmentation_results/first_seg_{sample_id}.nii.gz',
                'subcortical_volumes': 'vfs://home/tmp/BBMRI/fastr_subcortical_segmentation_results/vol_{sample_id}.txt',
                'QC_report':           'vfs://home/tmp/BBMRI/fastr_subcortical_segmentation_results/slicesdir'}
    
    shutil.rmtree(sinkdata['QC_report'].replace('vfs://home','/home/mrphys/marzwi'), ignore_errors=True)

    return sinkdata


def create_network():
    
    # Create the network nodes
    network        = fastr.Network(                           id_='subcortical_segmentation')
    t1_source      = network.create_source('DicomImageFile',  id_='t1_source')
    label_source   = network.create_source('Int',             id_='label_source')
    edge_source    = network.create_constant('Float', [0.5],  id_='bin_edge')
    dcm2nii_node   = network.create_node('DicomToNifti',      id_='dicom_to_nifti')
    first_seg_node = network.create_node('RunFirstAll',       id_='first_segmentation')
    add_node       = network.create_node('Add',               id_='add')
    subtract_node  = network.create_node('Subtract',          id_='subtract')
    volume_node    = network.create_node('FSLStats',          id_='volume_stats')
    slicesdir_node = network.create_node('FirstROISlicesDir', id_='slicesdir_report')
    volumesum_node = network.create_node('FSLStatsVolume',    id_='aggregate_volumes')
    volume_sink    = network.create_sink('TxtFile',           id_='subcortical_volumes')
    slicesdir_sink = network.create_sink('Directory',         id_='QC_report')
    
    # Set the datatype of the sources/constants to avoid complains from fastr (this shouldn't be necessary, really (can be figured out at runtime or by propagation))
    dcm2nii_node.outputs['image_cropped'].datatype = fastr.typelist['NiftiImageFileCompressed']
    
    # Create the network links
    # Volume = fslstats output_name_all_fast_firstseg -l 16.5 -u 17.5 -V where the first number of the output is the number of voxels and the second is the volume in mm3
    label_source.output                    >> subtract_node.inputs['left_hand']
    edge_source.output                     >> subtract_node.inputs['right_hand']
    label_source.output                    >> add_node.inputs['left_hand']
    edge_source.output                     >> add_node.inputs['right_hand']
    t1_source.output                       >> dcm2nii_node.inputs['dicom_image']
    dcm2nii_node.outputs['image_cropped']  >> first_seg_node.inputs['t1_image']
    first_seg_node.outputs['firstseg']     >> volume_node.inputs['image']
    subtract_node.outputs['result']        >> volume_node.inputs['lower_threshold']
    add_node.outputs['result']             >> volume_node.inputs['upper_threshold']
    (dcm2nii_node.outputs['image_cropped'] >> slicesdir_node.inputs['t1_image']).collapse    = 't1_source'
    (first_seg_node.outputs['firstseg']    >> slicesdir_node.inputs['label_image']).collapse = 't1_source'
    (volume_node.outputs['nonzero_volume'] >> volumesum_node.inputs['volume_stats']).collapse = 't1_source'
    volumesum_node.outputs['volume_csv']   >> volume_sink.input
    slicesdir_node.outputs['report']       >> slicesdir_sink.input
    
    # Manage the data flow
    volume_node.inputs['image'].input_group = 't1_image'
    
    return network


def main():
    
    # Get the source- and sink-data and execute the network
    sourcedata = source_data()
    sinkdata   = sink_data()
    network    = create_network()
    network.draw_network(name = network.id, draw_dimension = True)
    network.execute(sourcedata, sinkdata, tmpdir='/home/mrphys/marzwi/tmp/BBMRI/fastr_subcortical_segmentation')


if __name__ == '__main__':
    main()
