
import fastr 
import os
import datetime 

def source_data(Source_Name, Sample_Name, Model_Name, Output_Base_Dir):
	
	# This builds the input data for the ICV node. 
	# Here you can change values of the parameters (not recommended). You can also change them in the parameters file. 
	# However, values here replace the values in the parameters files. 
	# For detailed description of the different parameters - see the documentations or the tool (ICV.xml) definition file. 
	
	# Possible paramaters:
	# '-h' Print a basic help txt for the program
	# 'Input ICV model': ... ,  Not required. Can be set to: Three_Tesla_Adults_Model or Three_Tesla_Kids_Model or OneHalf_Tesla_Adults_Model or OneHalf_Tesla_Kids_Model or Custom_Model. In the last case the progeram also requires a string with the uri of the custom model files.    
	# 'Parameters_File': ... , Not required. If absent the program will read the default parameter file at parameter folder in the tool home directory 
	# 'Processed_File': ... , It is required to set either Processed_File or Processed_Folder
	# 'Processed_Folder': ... , It is required to set either Processed_File or Processed_Folder 
	# 'Num_Of_Registrations': ... Not required. Overrides a value from the parameter file
	# 'Registration_Blur_FWHM': ... , Not required. Overrides a value from the parameter file
	# 'Num_Of_Itterations': ... , Not required. Overrides a value from the parameter file
	# 'Step_Size': ..., Not required. Overrides a value from the parameter file
	# 'Blur_FWHM': ..., Not required. Overrides a value from the parameter file
	# 'Weight': ..., Not required. Overrides a value from the parameter file
	# 'Stiffness': ..., Not required. Overrides a value from the parameter file
	# 'Similarity': ... , Not required. Overrides a value from the parameter file
	# 'Sub_lattice': ... , Not required. Overrides a value from the parameter file
	# 'Debug':  ... ,  Not required. Overrides a value from the parameter file
	# 'Clobber': ...,  Not required. Overrides a value from the parameter file
	# 'Non_linear': ... ,  Not required. Overrides a value from the parameter file
	# 'Correction_Coefficient': .... , Not required. Overrides a value from the parameter file
	# 'Mask_To_Binary_Min': .... ,  Not required. Overrides a value from the parameter file
	# 'RUN_GUI': ... , Can be either True or False. If False one of the following three values: Calc_Crude, Calc_Fine or Clac_Ave_Brain must exist and be True. 
	# 'Calc_Crude': ... , Calculate the ICV mask on a more crude level. For most purpose is enough. See remark above in 'RUN_GUI'
	# 'Calc_Fine': ... , Calculate the ICV mask in detailed. See remark above in 'RUN_GUI' 
	# 'Clac_Ave_Brain': .... , Calculate an average MRI brain image from couple of subjects. See remark above in 'RUN_GUI'
	# 'ICV_Output_Directory'.... , A Folder that contains all the outputs files 
	
	Split_Sample_Name=Sample_Name.split('.',Sample_Name.count('.'))
	Sample_ID=Split_Sample_Name [0]
	Final_Output_Dir_Address = Sample_ID +'_Analysis/'
	
	sourcedata = {
			'Model': Model_Name,
			'Processed_File': Source_Name + '/' + Sample_Name,
			'Processed_Folder': Source_Name ,
			'ICV_Subject_Name' : Split_Sample_Name [0],
			'Num_Of_Registrations': 2, 
			'Registration_Blur_FWHM': 1,
			'Num_Of_Iterations': 5,
			'Step_Size' : 16,
			'Blur_FWHM' : 8,
			'Weight':1.0, 
			'Stiffness':1.0, 
			'Similarity': 0.3, 
			'Sub_lattice': 6, 
			'Debug': True, 
			'Clobber': True, 
			'Non_linear': True, 
			'Correction_Coefficient': True, 
			'Mask_To_Binary_Min': 0.5,
			'Calc_Crude': True,
			'ICV_Output_Directory': Output_Base_Dir + '/' + Sample_ID + '/' + Final_Output_Dir_Address ,
			}
	return sourcedata 

def sink_data(Output_Base_Dir,Sample_Name):
	
	# This builds the output data for the ICV program
	# 'ICV_Mask': .... , The Intracranial Volume mask that the program created
	# 'ICV_Volume': .... , A Biomarker text file with the size in mm of the Intracranial Volume
	# 'Used_Parameters': .... , The parameters that were used to create the Intracranial Volume mask
	# 'ICV_Output_Log'': .... ,  A log file with all the processes and bash commands that the program produced
	
	st=datetime.datetime.now().strftime("%Y_%m_%d_%H_%M%p")
	Split_Sample_Name=Sample_Name.split('.',Sample_Name.count('.'))
	Sample_ID=Split_Sample_Name [0]
	Final_Output_Dir_Address ='/'+ Sample_ID +'_Analysis/'
	
	if len(Split_Sample_Name)>1:
		if Split_Sample_Name[1]=='mnc':
			Suffix='mnc'
		else:
			Suffix='nii'
	else: 
		Suffix='nii'
		
	sinkdata = {
			'ICV_Output_ICV_Mask' : Output_Base_Dir + '/' + Sample_ID + Final_Output_Dir_Address + Sample_ID + '_ICV_Mask_' + st + '.' + Suffix,
			'ICV_Output_ICV_Volume' : Output_Base_Dir + '/' + Sample_ID + Final_Output_Dir_Address + Sample_ID +'_ICV_Mask_Statistics_' +st + '.txt',
			'ICV_Output_ICV_Parameters' : Output_Base_Dir +  '/' + Sample_ID + Final_Output_Dir_Address + Sample_ID +'_ICV_Parameters_' + st + '.txt',
			'ICV_Output_Log': Output_Base_Dir +  '/' + Sample_ID + Final_Output_Dir_Address + Sample_ID + '_log_' + st + '.txt',	
			}  
	return sinkdata


def create_network(Input_Mode):
	
	# Building up the ICV Node
	network = fastr.Network(id_='IntracranialVolume')
	ICV_Build_Model = network.create_node(fastr.toollist['ICV'],id_='IntracranialVolume',stepid='Main_Operation')	
	
	# Building up the ICV input part of the Network		
	ICV_Model = network.create_source(fastr.typelist['String'],id_='Model',stepid='Model_Name')	
	
	if Input_Mode == 'Folder':
		ICV_Input_Folder = network.create_source(fastr.typelist['Directory'],id_='Processed_Folder',stepid='Program_File_Arguments')
	elif Input_Mode == 'Files':
		ICV_Input_File = network.create_source(fastr.typelist['ICVInputFiletypes'],id_='Processed_File',stepid='Program_File_Arguments')
	
	ICV_Subject_Name = network.create_source (fastr.typelist['String'],id_='ICV_Subject_Name',stepid='Subject_Name')			
	
	ICV_Num_Of_Registrations = network.create_source(fastr.typelist['Int'],id_='Num_Of_Registrations',stepid='Program_Linear_Registration_Arguments')	
	ICV_Registration_Blur_FWHM = network.create_source(fastr.typelist['Int'],id_='Registration_Blur_FWHM',stepid='Program_Linear_Registration_Arguments')
	
	ICV_Number_Of_Iterations = network.create_source(fastr.typelist['Int'],id_='Num_Of_Iterations',stepid='Program_Basic_Arguments')
	ICV_Step_Size = network.create_source(fastr.typelist['Int'],id_='Step_Size',stepid='Program_Basic_Arguments')
	ICV_Blur_FWHM = network.create_source(fastr.typelist['Int'],id_='Blur_FWHM',stepid='Program_Basic_Arguments')
	
	ICV_Weight = network.create_source(fastr.typelist['Float'],id_='Weight',stepid='Program_Extended_Arguments')
	ICV_Stiffness = network.create_source(fastr.typelist['Float'],id_='Stiffness',stepid='Program_Extended_Arguments')
	ICV_Similarity = network.create_source(fastr.typelist['Float'],id_='Similarity',stepid='Program_Extended_Arguments')
	ICV_Sub_lattice = network.create_source(fastr.typelist['Int'],id_='Sub_lattice',stepid='Program_Extended_Arguments')
	ICV_Debug = network.create_source(fastr.typelist['Boolean'],id_='Debug',stepid='Program_Extended_Arguments')
	ICV_Clobber = network.create_source(fastr.typelist['Boolean'],id_='Clobber',stepid='Program_Extended_Arguments')
	ICV_Non_linear = network.create_source(fastr.typelist['Boolean'],id_='Non_linear',stepid='Program_Extended_Arguments')
	ICV_Correction_Coefficient = network.create_source(fastr.typelist['Boolean'],id_='Correction_Coefficient',stepid='Program_Extended_Arguments')
	
	ICV_Mask_To_Binary_Min = network.create_source(fastr.typelist['Float'],id_='Mask_To_Binary_Min',stepid='Program_Extended_Arguments')

	ICV_Operation_Type = network.create_source(fastr.typelist['Boolean'],id_='Calc_Crude',stepid='Operation_Type')
	
	ICV_Input_link2 = network.create_link(ICV_Model.output, ICV_Build_Model.inputs['Model'])
	if Input_Mode == 'Folder':
		ICV_Input_link4 = network.create_link(ICV_Input_Folder.output, ICV_Build_Model.inputs['Processed_Folder'])
	elif Input_Mode == 'Files':
		ICV_Input_link3 = network.create_link(ICV_Input_File.output, ICV_Build_Model.inputs['Processed_File'])
	
	ICV_Input_link7 = network.create_link(ICV_Num_Of_Registrations.output, ICV_Build_Model.inputs['Num_Of_Registrations'])
	ICV_Input_link8 = network.create_link(ICV_Registration_Blur_FWHM.output, ICV_Build_Model.inputs['Registration_Blur_FWHM'])
	
	ICV_Input_link9 = network.create_link(ICV_Number_Of_Iterations.output, ICV_Build_Model.inputs['Num_Of_Iterations'])
	ICV_Input_link10 = network.create_link(ICV_Step_Size.output, ICV_Build_Model.inputs['Step_Size'])
	ICV_Input_link11 = network.create_link(ICV_Blur_FWHM.output, ICV_Build_Model.inputs['Blur_FWHM'])
	
	ICV_Input_Link12 = network.create_link(ICV_Weight.output, ICV_Build_Model.inputs['Weight'])
	ICV_Input_Link13 = network.create_link(ICV_Stiffness.output, ICV_Build_Model.inputs['Stiffness'])
	ICV_Input_Link14 = network.create_link(ICV_Similarity.output, ICV_Build_Model.inputs['Similarity'])
	ICV_Input_Link15 = network.create_link(ICV_Sub_lattice.output, ICV_Build_Model.inputs['Sub_lattice'])
	ICV_Input_Link16 = network.create_link(ICV_Debug.output, ICV_Build_Model.inputs['Debug'])
	ICV_Input_Link17 = network.create_link(ICV_Clobber.output, ICV_Build_Model.inputs['Clobber'])
	ICV_Input_Link18 = network.create_link(ICV_Non_linear.output, ICV_Build_Model.inputs['Non_linear'])
	ICV_Input_Link19 = network.create_link(ICV_Correction_Coefficient.output, ICV_Build_Model.inputs['Correction_Coefficient']) 
	ICV_Input_Link20 = network.create_link(ICV_Mask_To_Binary_Min.output, ICV_Build_Model.inputs['Mask_To_Binary_Min'])   
	
	ICV_Input_link22 = network.create_link(ICV_Operation_Type.output, ICV_Build_Model.inputs['Calc_Crude'])
	
	ICV_Input_link24 = network.create_link(ICV_Subject_Name.output, ICV_Build_Model.inputs['Subject_Name'])
	
				
	ICV_Output_Directory = network.create_source (fastr.typelist['Directory'],id_='ICV_Output_Directory',stepid='ICV_Directory_Outputs')	
	ICV_Output_ICV_Mask = network.create_sink (fastr.typelist['ICVOutputFiletypes'],id_='ICV_Output_ICV_Mask',stepid='ICV_Files_Outputs')
	ICV_Output_ICV_Parameters = network.create_sink (fastr.typelist['TxtFile'],id_='ICV_Output_ICV_Parameters',stepid='ICV_Files_Outputs')	
	ICV_Output_ICV_Mask_BioMarker = network.create_sink (fastr.typelist['TxtFile'],id_='ICV_Output_ICV_Volume',stepid='ICV_Files_Outputs')	
	ICV_Output_ICV_Log = network.create_sink (fastr.typelist['TxtFile'],id_='ICV_Output_Log',stepid='ICV_Files_Outputs')	

	ICV_Output_link1 = network.create_link(ICV_Output_Directory.output, ICV_Build_Model.inputs['icv_directory'])
	
	ICV_Output_link2 = network.create_link(ICV_Build_Model.outputs['icv_output_mask'],ICV_Output_ICV_Mask.input)	
	ICV_Output_link5 = network.create_link(ICV_Build_Model.outputs['icv_parameters_used'],ICV_Output_ICV_Parameters.input)
	ICV_Output_link6 = network.create_link(ICV_Build_Model.outputs['icv_volume'],ICV_Output_ICV_Mask_BioMarker.input)
	ICV_Output_link7 = network.create_link(ICV_Build_Model.outputs['icv_log'],ICV_Output_ICV_Log.input)					
									 
	return network

def main():
	
	# Define the parameters that will be used to build the network:
	Temp_Dir='vfs://tmp/ICV_New_Data' # Directory to put the fastr processing results. Should be changed to the appropriate directory accordingly
	
	Source_Name='xnat://xnat.bmia.nl/data/projects/bbmri-wp3-dev/subjects/UMCU00009/experiments/BMIAXNAT_E05075/scans/301/resources/DICOM/files?format=zip'
	#Source_Name='vfs://icv_data/EMC00056_20100511_1315_MRI_HERS_OMM/scans/6/DICOM'  # The source of the fil
	Output_Base_Dir='vfs://icv_data/EMC00056_20100511_1315_MRI_HERS_OMM/scans/6/DICOM' # The Based directory of the results.
	Sample_Name='E05072.dcm' #The name of the file to process results. Should be changed when fastr XNAT grabber is added. 
	
	Model_Name='OneHalf_Tesla_Adults_Model' # The model to process. Should be either OneHalf_Tesla_Adults_Model, OneHalf_Tesla_Kids_Model, Three_Tesla_Adults_Model, Three_Tesla_Kids_Model 
											# or a string with the address of the custom ICV model (without the vfs:// prefix)
	
	Network_Input_Mode='Folder' # Can be either 'File' or 'Folder'. The First case applies if one wants to process a single mnc or nii file. The second case applies if one wants to process a fol
	
	# Get the source and sink data 
	sourcedata = source_data(Source_Name,Sample_Name, Model_Name, Output_Base_Dir)
	sinkdata   = sink_data(Output_Base_Dir,Sample_Name)
	
	# Build the network
	network    = create_network(Network_Input_Mode)
	
	# Print the source and the sink data
	print 'The source data is:\n'
	print sourcedata
	print '\n'
	print 'The sink data is:\n'
	print sinkdata
	print '\n'
	
    # Execute the network    
	network.draw_network(name = network.id, img_format='svg',draw_dimension = True)
	#network.execute (sourcedata,sinkdata,tmpdir=Temp_Dir)
	network.execute (sourcedata,sinkdata)
		
if __name__ == '__main__':
    main();

