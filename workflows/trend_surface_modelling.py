# -*- coding: utf-8 -*-
"""
Created on Wed Aug 16 13:42:51 2017

@author: Seyed Mostafa Kia (m.kia83@gmail.com), Marcel Zwiers (m.zwiers@donders.ru.nl)
"""

#!/usr/bin/env python

import fastr
import shutil

def source_data():
    sourcedata = {'fmri_images':   {'S1': '/home/mrstats/seykia/Data/trendsurf/lh.striatum.evec.1.nii.gz'},
                  'mask':          '/home/mrstats/seykia/Data/trendsurf/lh.mask.nii.gz',
                  'basis':          3
                 }   
    return sourcedata


def sink_data():
    sinkdata = {'results': 'vfs://home/Results/trendsurf/subject_{sample_id}'}
    shutil.rmtree(sinkdata['results'].replace('vfs://home','/home/mrstats/seykia'), ignore_errors=True)
    return sinkdata


def create_network():
    # Creating the nodes
    network             = fastr.Network(                            id_='trend_surface_modelling') 
    fmri_source         = network.create_source('NiftiImageFile',   id_='fmri_images',          stepid='TSM')
    mask_source         = network.create_source('NiftiImageFile',   id_='mask',                 stepid='TSM')
    basis_source        = network.create_source('Int',              id_='basis',                stepid='TSM')
    tsm_node            = network.create_node('trendsurf',          id_='tsm',                  stepid='TSM')
    tsm_sink            = network.create_sink('Directory',          id_='results',              stepid='TSM')

    # Set the datatype of the sources to avoid complains from fastr (this shouldn't be necessary, really (can be figured out at runtime or by propagation))
    mask_source.output.datatype                               =   fastr.typelist['NiftiImageFile']
    
    # Creating the links
    fmri_source.output                                  >>  tsm_node.inputs['fmri_images']
    mask_source.output                                  >>  tsm_node.inputs['mask']    
    basis_source.output                                 >>  tsm_node.inputs['basis']
    tsm_node.outputs['output_dir']                      >>  tsm_sink.input
    
    return network


def main():
    sourcedata = source_data()
    sinkdata   = sink_data()
    network    = create_network()
    network.draw_network(name = network.id, draw_dimension = True)
    network.execute(sourcedata, sinkdata, tmpdir='/home/mrstats/seykia/temp/trendsurf')


if __name__ == '__main__':
    main()
