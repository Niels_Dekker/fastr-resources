#!/usr/bin/env python

import fastr
import shutil
import argparse
import json
# import glob

# Defaults
CMA_labels = {'Left-Thalamus-Proper':     10,
              'Left-Caudate':             11,
              'Left-Putamen':             12,
              'Left-Pallidum':            13,
              'Brain-Stem_4th-Ventricle': 16,
              'Left-Hippocampus':         17,
              'Left-Amygdala':            18,
              'Left-Accumbens-area':      26,
              'Right-Thalamus-Proper':    49,
              'Right-Caudate':            50,
              'Right-Putamen':            51,
              'Right-Pallidum':           52,
              'Right-Hippocampus':        53,
              'Right-Amygdala':           54,
              'Right-Accumbens-area':     58}


def source_data(dict_file, experiments):

    with open(dict_file) as input_file:
        data = json.load(input_file)
    sourcedata = { 't1_source': {}, 'label_source': CMA_labels }
    
    for experiment in experiments:
        if experiment in data:
            experiment_data = data[experiment]
            print experiment
            if 't1w_dicom' in experiment_data:
                sourcedata['t1_source'][experiment] = experiment_data['t1w_dicom'] 
    
    return sourcedata


def sink_data(basedir):
    
    sinkdata = {'first_seg_image':     basedir + '/{sample_id}/first_seg_{sample_id}.nii.gz',
                'subcortical_volumes': basedir + '/{sample_id}/vol_{sample_id}.txt',
                'QC_report':           basedir + '/{sample_id}/slicesdir'}
    
    #shutil.rmtree(sinkdata['QC_report'].replace('vfs://home','/home/mrphys/marzwi'), ignore_errors=True)

    return sinkdata


def create_network():
    
    # Create the network nodes
    network        = fastr.Network(                           id_='subcortical_segmentation')
    t1_source      = network.create_source('DicomImageFile',  id_='t1_source')
    label_source   = network.create_source('Int',             id_='label_source')
    edge_source    = network.create_constant('Float', [0.5],  id_='bin_edge')
    dcm2nii_node   = network.create_node('DicomToNiftiLongPath', id_='dicom_to_nifti')
    first_seg_node = network.create_node('RunFirstAllSGE',    id_='first_segmentation')
    add_node       = network.create_node('Add',               id_='add')
    subtract_node  = network.create_node('Subtract',          id_='subtract')
    volume_node    = network.create_node('FSLStats',          id_='volume_stats')
    volume_node.inputs['image'].input_group = 't1_image'
    slicesdir_node = network.create_node('FirstROISlicesDir', id_='slicesdir_report')
    volumesum_node = network.create_node('FSLStatsVolume',    id_='aggregate_volumes')
    volume_sink    = network.create_sink('TxtFile',           id_='subcortical_volumes')
    slicesdir_sink = network.create_sink('Directory',         id_='QC_report')

    #Memory requirements
    first_seg_node.required_memory = '8g'
    
    # Set the datatype of the sources/constants to avoid complains from fastr (this shouldn't be necessary, really (can be figured out at runtime or by propagation))
    dcm2nii_node.outputs['image_cropped'].datatype = fastr.typelist['NiftiImageFileCompressed']
    
    # Create the network links
    # Volume = fslstats output_name_all_fast_firstseg -l 16.5 -u 17.5 -V where the first number of the output is the number of voxels and the second is the volume in mm3
    label_source.output                    >> subtract_node.inputs['left_hand']
    edge_source.output                     >> subtract_node.inputs['right_hand']
    label_source.output                    >> add_node.inputs['left_hand']
    edge_source.output                     >> add_node.inputs['right_hand']
    t1_source.output                       >> dcm2nii_node.inputs['dicom_image']
    dcm2nii_node.outputs['image_cropped']  >> first_seg_node.inputs['t1_image']
    first_seg_node.outputs['firstseg']     >> volume_node.inputs['image']
    subtract_node.outputs['result']        >> volume_node.inputs['lower_threshold']
    add_node.outputs['result']             >> volume_node.inputs['upper_threshold']
    (dcm2nii_node.outputs['image_cropped'] >> slicesdir_node.inputs['t1_image']).collapse    = 't1_source'
    (first_seg_node.outputs['firstseg']    >> slicesdir_node.inputs['label_image']).collapse = 't1_source'
    (volume_node.outputs['nonzero_volume'] >> volumesum_node.inputs['volume_stats']).collapse = 'label_source'
    volumesum_node.outputs['volume_csv']   >> volume_sink.input
    slicesdir_node.outputs['report']       >> slicesdir_sink.input
    
    return network


def main():
    # Parsing arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--subjects', type=unicode, nargs='+', required=True, help='subject name')
    parser.add_argument('--dict', type=unicode, required=True, help='Dictionary of source data')
    parser.add_argument('--temp', type=unicode, required=True, help='temp diretory')
    parser.add_argument('--out', type=unicode, required=True, help='output directory')
    args = parser.parse_args()

    # Get the source- and sink-data and execute the network
    sourcedata = source_data(args.dict, args.subjects)
    sinkdata   = sink_data(args.out)
    print('source data:')
    for sample in sourcedata['t1_source']:
        print('  {}'.format(sample))

    network    = create_network()
    network.draw_network(name = network.id, draw_dimension = True)
    network.execute(sourcedata, sinkdata, tmpdir=args.temp)


if __name__ == '__main__':
    main()
