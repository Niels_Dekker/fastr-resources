#!/usr/bin/env python

import fastr
# import glob

# Defaults
kernel = 5         # WM/CSF erosion kernel box <size> : all voxels in a box of width <size> centered on target voxel
bptf   = [50, -1]  # Highpass/lowpass filter frequency: high/lowpass_sigma = halfmax / TR = 0.5 / (high/lowpass_freq * TR) => bptf = 0.5 / high/lowpass_freq


def source_data():
    
    # S1_rs_fMRI = tuple(f.replace('/home/mrphys/marzwi','vfs://home') for f in glob.glob("/home/mrphys/marzwi/BIG/niftibox/rs_fMRI/BIG1070/1/BIG1070_1*.nii"))
    # S2_rs_fMRI = tuple(f.replace('/home/mrphys/marzwi','vfs://home') for f in glob.glob("/home/mrphys/marzwi/BIG/niftibox/rs_fMRI/BIG1094/1/BIG1094_1*.nii"))
    sourcedata = {'fmri_images':  {'S1': 'vfs://home/MATRICS/sandbox/001-001/13_rs_fMRI',
                                   'S2': 'vfs://home/MATRICS/sandbox/001-003/25_rs_fMRI'},
                  'fmri_tr':        2.0,
                  't1_image':     {'S1': 'vfs://home/MATRICS/dropbox/001-001/MRI/02_t1_structural',
                                   'S2': 'vfs://home/MATRICS/dropbox/001-003/MRI/14_t1_structural'},
                  'master_setup':  'vfs://home/python/bbmri/fastr-resources/workflows/rs_fMRI.fsf',
                  'ica_templates': 'vfs://home/BIG/niftibox/rs_fMRI/PNAS_Smith09_rsn10.nii'}
    
    return sourcedata


def sink_data():
    
    sinkdata = {'dualreg_results': 'vfs://home/tmp/BBMRI/fastr_dual_regression_results/dual_regression_{sample_id}'}
    
    return sinkdata


def create_network():
    
    # Create the network nodes
    network         = fastr.Network(                          id_='dual_regression')
    fmri_source     = network.create_source('DicomImageFile', id_='fmri_images',           stepid='fmri_preprocessing')
    tr_source       = network.create_source('Float',          id_='fmri_tr',               stepid='fmri_preprocessing')
    feat_source     = network.create_source('FsfFile',        id_='master_setup',          stepid='fmri_preprocessing')
    bptf_constant   = network.create_constant('Float', bptf,  id_='bandpass_halfmax',      stepid='fmri_preprocessing')
    dcm2nii_node    = network.create_node('DicomToNifti',     id_='dicom_to_nifti',        stepid='fmri_preprocessing')
    reorient_node   = network.create_node('FSLReorient2Std',  id_='reorient2std',          stepid='fmri_preprocessing')
    feat_node       = network.create_node('Feat',             id_='feat_preprocessing',    stepid='fmri_preprocessing')
    aroma_node      = network.create_node('ICAAroma',         id_='ica_aroma_denoising',   stepid='fmri_preprocessing')
    filter_node     = network.create_node('FSLMaths',         id_='maths_bandpass_smooth', stepid='fmri_preprocessing')
    divide_node     = network.create_node('Divide',           id_='bandpass_sigma',        stepid='fmri_preprocessing')
    t1_source       = network.create_source('DicomImageFile', id_='t1_image',              stepid='partial_volume_maps')
    dcm2nii_T1_node = network.create_node('DicomToNifti',     id_='dicom_to_nifti_T1',     stepid='partial_volume_maps')
    merge_pves_node = network.create_node('FSLMerge',         id_='merge_pves',            stepid='partial_volume_maps')
    anat_node       = network.create_node('FSLAnat',          id_='anat_preprocessing',    stepid='partial_volume_maps')
    erode_node      = network.create_node('FSLMaths',         id_='maths_erode',           stepid='partial_volume_maps')
    warp_pves_node  = network.create_node('ApplyWarp',        id_='apply_warp_pves',       stepid='partial_volume_maps')
    ica_maps_source = network.create_source('NiftiImageFile', id_='ica_templates',         stepid='group_ica_maps')
    warp_maps_node  = network.create_node('ApplyWarp',        id_='apply_warp_ica',        stepid='group_ica_maps')
    merge_maps_node = network.create_node('FSLMerge',         id_='merge_maps',            stepid='dual_regression')
    dualreg_node    = network.create_node('DualRegression',   id_='dual_regression',       stepid='dual_regression')
    dualreg_sink    = network.create_sink('Directory',        id_='dualreg_results',       stepid='dual_regression')
    
    # Set the datatype of the sources/constants to avoid complains from fastr (this shouldn't be necessary, really (can be figured out at runtime or by propagation))
    dcm2nii_node.outputs['image'].datatype             = fastr.typelist['NiftiImageFileCompressed']
    dcm2nii_T1_node.outputs['image_cropped'].datatype  = fastr.typelist['NiftiImageFileCompressed']
    reorient_node.outputs['reoriented_image'].datatype = fastr.typelist['NiftiImageFileCompressed']
    ica_maps_source.output.datatype                    = fastr.typelist['NiftiImageFileUncompressed']
    divide_node.outputs['result'].datatype             = fastr.typelist['Float']   # >> filter_node.inputs['operator1_number']
    filter_node.inputs['operator2_number'].datatype    = fastr.typelist['Float']
    erode_node.inputs['operator1_number'].datatype     = fastr.typelist['Float']
    
    # Create the network links
    fmri_source.output                                >> dcm2nii_node.inputs['dicom_image']
    t1_source.output                                  >> dcm2nii_T1_node.inputs['dicom_image']
    dcm2nii_T1_node.outputs['image_cropped']          >> anat_node.inputs['image']
    (True,)                                           >> anat_node.inputs['no_subcort_seg']
    dcm2nii_node.outputs['image']                     >> reorient_node.inputs['image']
    reorient_node.outputs['reoriented_image']         >> feat_node.inputs['fmri_images']
    anat_node.outputs['biascorr_brain_image']         >> feat_node.inputs['t1_brain_image']
    # anat_node.outputs['biascorr_image']               >> feat_node.inputs['t1_image']
    feat_source.output                                >> feat_node.inputs['setup_file']
    tr_source.output                                  >> feat_node.inputs['TR']
    feat_node.outputs['feat_directory']               >> aroma_node.inputs['feat_directory']
    bptf_constant.output                              >> divide_node.inputs['left_hand']
    tr_source.output                                  >> divide_node.inputs['right_hand']
    aroma_node.outputs['denoised_nonaggr_images']     >> filter_node.inputs['image1']
    ('-bptf',)                                        >> filter_node.inputs['operator1']
    (divide_node.outputs['result']                    >> filter_node.inputs['operator1_number']).collapse = 'bandpass_halfmax'
    ('-s',)                                           >> filter_node.inputs['operator2']
    (kernel,)                                         >> filter_node.inputs['operator2_number']
    (1,)                                              >> merge_pves_node.inputs['t_flag']
    (anat_node.outputs['fast_pve_0_image'],
     anat_node.outputs['fast_pve_2_image'])           >> merge_pves_node.inputs['images']
    merge_pves_node.outputs['image']                  >> erode_node.inputs['image1']
    ('-kernel',)                                      >> erode_node.inputs['operator1']
    ('box',)                                          >> erode_node.inputs['operator1_string']
    (kernel,)                                         >> erode_node.inputs['operator1_number']
    ('-ero',)                                         >> erode_node.inputs['operator2']
    erode_node.outputs['output_image']                >> warp_pves_node.inputs['source_image']
    feat_node.outputs['example_image']                >> warp_pves_node.inputs['reference_image']
    feat_node.outputs['highres2example_func']         >> warp_pves_node.inputs['pre_mat']
    ica_maps_source.output                            >> warp_maps_node.inputs['source_image']
    feat_node.outputs['example_image']                >> warp_maps_node.inputs['reference_image']
    anat_node.outputs['MNI_to_T1_nonlin_field_image'] >> warp_maps_node.inputs['warp_coef_image']
    feat_node.outputs['highres2example_func']         >> warp_maps_node.inputs['post_mat']
    (1,)                                              >> merge_maps_node.inputs['t_flag']
    warp_pves_node.outputs['warped_image']            >> merge_maps_node.inputs['images']
    warp_maps_node.outputs['warped_image']            >> merge_maps_node.inputs['images_']
    merge_maps_node.outputs['image']                  >> dualreg_node.inputs['group_ica_maps']
    filter_node.outputs['output_image']               >> dualreg_node.inputs['fmri_images']
    dualreg_node.outputs['output_directory']          >> dualreg_sink.input
    
    return network


def main():
    
    # Get the source- and sink-data and execute the network
    sourcedata = source_data()
    sinkdata   = sink_data()
    network    = create_network()
    network.draw_network(name = network.id, draw_dimension = True)
    network.execute(sourcedata, sinkdata, tmpdir='/home/mrphys/marzwi/tmp/BBMRI/fastr_dual_regression/')


if __name__ == '__main__':
    main()
