from fastr.datatypes import URLType


class CompressedMincImageFile(URLType):
    description = 'Compressed MINC Image format'
    extension = 'mnc.gz'
