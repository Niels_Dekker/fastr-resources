from fastr.datatypes import URLType

class FsfFile(URLType):
    description = 'FSL FEAT set-up file'
    extension = 'fsf'
