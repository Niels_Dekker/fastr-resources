from fastr.datatypes import TypeGroup


class ICVInputFiletypes(TypeGroup):
    description = 'ICV Input Files Types'
    _members = frozenset(['NiftiImageFile',
                          'MincImageFile',
			  'DicomImageFile'])
