from fastr.datatypes import TypeGroup


class ICVOutputFiletypes(TypeGroup):
    description = 'ICV Output File Types'
    _members = frozenset(['NiftiImageFile',
                          'MincImageFile',
			  'CompressedMincImageFile'])
